# onug-monitoring

This repository includes instructions for setting up a network monitoring stack and pointing it at a demo network environment.

It's intended to showcase a flexible approach to monitoring modern networks that can also be used for other systems such as storage, server and application metrics.


## Environment setup

We want something to monitor, so we'll go ahead and stand up a leaf-spine network using docker containers.
This setup will require a small workspace and comprises several integrated components, so we'll create a small folder heirarchy:
```
mkdir ~/promisenmp/
mkdir ~/promisenmp/tmp/
mkdir ~/promisenmp/monitoring
```

We have one small snmp-related change to make to our config, so we'll clone our environment and drop this in:
```
git clone https://gitlab.com/depereo/cloudbuilders-demo ~/promisenmp/env
cd ~/promisenmp/env/
find ~/promisenmp/env/bootstrap/ -type f -exec sh -c "echo -e '\nsnmp-server community public' >> {}" \;
```

Setup for this part includes a little bit of prep with regards to downloading images, so if you haven't done that part before go ahead and go through the guide here: https://gitlab.com/depereo/cloudbuilders-demo/
Then we can stand up the environment with `docker-compose up -d` and, once it's finished standing up configure it with `ansible-playbook ceos_config.yaml -i inventory-docker.py -u admin -k`.

## SNMP Monitoring of this environment with Prometheus

We're going to set up three components to gather metrics and store the data, as well as assist in alerting.
 * prometheus, which will collect metrics data as key-value pairs and stores it in a time-series database.
 * snmp_exporter, which will gather SNMP data from our environment and present it in a standard format for prometheus to collect.
 * alertmanager, which can let us know when failures have occurred.

### Create an SNMP configuration

We need to generate an SNMP configuration so that our exporter understands the MIBs we'll be querying. We'll also do this with an SNMPv2c community in the demo.

`git clone https://github.com/prometheus/snmp_exporter ~/promisenmp/tmp/snmp_exporter`

We're going to adjust ~/promisenmp/tmp/snmp_exporter/generator/generator.yml to add the SNMP community by modifying the arista section to add an `auth` parameter:
```
# Arista Networks
#
# Arista Networks MIBs can be found here: https://www.arista.com/en/support/product-documentation/arista-snmp-mibs
#
# https://www.arista.com/assets/data/docs/MIBS/ARISTA-ENTITY-SENSOR-MIB.txt
# https://www.arista.com/assets/data/docs/MIBS/ARISTA-SW-IP-FORWARDING-MIB.txt
#
# Tested on Arista DCS-7010T-48 switch
#
  arista_sw:
    walk:
      - sysUpTime
      - interfaces
      - ifXTable
      - 1.3.6.1.2.1.25.3.3.1.2 # hrProcessorLoad
      - 1.3.6.1.2.1.25.2.3.1.6 # hrStorageUsed
      - 1.3.6.1.4.1.30065.3.1.1 # aristaSwFwdIp
    overrides:
      ifType:
        type: EnumAsInfo
    auth:
      community: public
```

or you can download a generator.yml and drop it in with `wget https://gitlab.com/depereo/onug-monitoring/raw/master/generator.yml -O ~/promisenmp/tmp/snmp_exporter/generator/generator.yml`

Once that's done, execute the following to generate snmp.yml, which snmp_exporter will use to know how to query these devices via SNMP.
```
cd ~/promisenmp/tmp/snmp_exporter/generator/
make mibs
docker build -t snmp-generator .
docker run -ti \
  -v "${PWD}:/opt/" \
  snmp-generator generate
```

Next we're going to grab binaries for prometheus, the snmp exporter and alertmanager:

```
wget https://github.com/prometheus/snmp_exporter/releases/download/v0.16.1/snmp_exporter-0.16.1.linux-amd64.tar.gz -O - | tar -xz -C ~/promisenmp/monitoring/
wget https://github.com/prometheus/prometheus/releases/download/v2.15.2/prometheus-2.15.2.linux-amd64.tar.gz -O - | tar -xz -C ~/promisenmp/monitoring/
wget https://github.com/prometheus/alertmanager/releases/download/v0.20.0/alertmanager-0.20.0.linux-amd64.tar.gz -O - | tar -xz -C ~/promisenmp/monitoring/
```

Let's move the snmp.yml file we generated to where it is required:
`mv ~/promisenmp/tmp/snmp_exporter/generator/snmp.yml ~/promisenmp/monitoring/snmp_exporter-0.16.1.linux-amd64/snmp.yml`

Then we're going to configure prometheus.yml to monitor our switches via SNMP. Download a prometheus.yml config and drop it in with `wget https://gitlab.com/depereo/onug-monitoring/raw/master/prometheus.yml -O ~/promisenmp/monitoring/prometheus-2.15.2.linux-amd64/prometheus.yml`
This config features some re-writing of labels and targets - we're going to be talking to the snmp_exporter process and asking it to query for us, instead of directly reaching out to the switches.

Finally, prometheus will need some rules to announce when to consider something failed so that it can notify Alertmanager.
We'll set that up with a rules.yml file; `wget https://gitlab.com/depereo/onug-monitoring/raw/master/rules.yml -O ~/promisenmp/monitoring/prometheus-2.15.2.linux-amd64/rules.yml`

With prometheus & snmp exporter configured, and being happy enough with Alertmanager's defaults for now, let's go ahead and fire things up.

```
(cd ~/promisenmp/monitoring/alertmanager-0.20.0.linux-amd64 && ./alertmanager) & (cd ~/promisenmp/monitoring/prometheus-2.15.2.linux-amd64 && ./prometheus) & (cd ~/promisenmp/monitoring/snmp_exporter-0.16.1.linux-amd64 && ./snmp_exporter) &
```

That sorts our monitoring stack out, for now, so we're going to go ahead and get a tool to visualize the relevant data and provide some insights.

# Monitoring Frontend
```
$ docker run -d \
  -p 3000:3000 \
  --name=grafana \
  grafana/grafana
```

Open grafana in your browser `xdg-open "http://localhost:3000" &>/dev/null &` and log in with admin/admin.

Navigate to the 'add a data source' page and add prometheus `xdg-open "http://localhost:3000/datasources/new" &>/dev/null &`
I found I had to put in the docker network gateway IP here, which can be found with:
```
$ docker container inspect grafana | jq .[0].NetworkSettings.Gateway
"172.17.0.1"
```

![](img/grafana_add_prom.png)

Import our SNMP dashboard by hitting the 'import' button on the dashboard management page or go directly to importing with `xdg-open "http://localhost:3000/dashboard/import" &>/dev/null &`

![](img/grafana_import_dash.png)

You can either upload https://gitlab.com/depereo/onug-monitoring/raw/master/dashboard.json or paste the JSON in directly to the text box.

![](img/grafana_load_dash.png)

This should get you to the result of having a dashboard for your SNMP data, allowing you to visualize per-switch information.

![](img/grafana_dashboard.png)

And there's a rich ability to produce on-demand graphs

![](img/grafana_queries.png)
